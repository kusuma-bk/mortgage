package com.org.mortgage.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.mortgage.dto.CustomerDto;
import com.org.mortgage.dto.CustomerResponseDto;
import com.org.mortgage.entity.Account;
import com.org.mortgage.entity.Customer;
import com.org.mortgage.entity.Loan;
import com.org.mortgage.exception.CustomerNotPresent;
import com.org.mortgage.exception.PasswordIncorrect;
import com.org.mortgage.repository.AccountRepository;
import com.org.mortgage.repository.CustomerRepository;
import com.org.mortgage.repository.LoanRepository;
import com.org.mortgage.utility.CustomerUtility;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	LoanRepository loanRepository;

	@Override
	public CustomerResponseDto customerLogin(CustomerDto customerDto) throws CustomerNotPresent, PasswordIncorrect {
		Optional<Customer> customerDetails = customerRepository.findByCustomerId(customerDto.getCustomerId());
		CustomerResponseDto customerResponseDto = new CustomerResponseDto();
		if (!(customerDetails.isPresent())) {
			throw new CustomerNotPresent(CustomerUtility.CUSTOMER_NOT_PRESENT);
		}

		if (!(customerDetails.get().getPassword().equals(customerDto.getPassword()))) {
			throw new PasswordIncorrect(CustomerUtility.PWD_NOT_PRESENT);
		}

		Optional<Account> accountDetails = accountRepository.findByUserId(customerDetails.get().getUserId());
		if (accountDetails.isPresent()) {
			Optional<Loan> loanDetails = loanRepository.findByUserId((customerDetails.get().getUserId()));
			if (loanDetails.isPresent()) {
				customerResponseDto.setMortgageAmount(loanDetails.get().getMortgageAmount());
				customerResponseDto.setSavingsAccount(accountDetails.get().getSavingsAccount());
				customerResponseDto.setSavingsAmount(accountDetails.get().getSavingsAmount());
				customerResponseDto.setEmiAmount(loanDetails.get().getEmiAmount());
				customerResponseDto.setUserId(accountDetails.get().getUserId());
				customerResponseDto.setCustomerId(customerDto.getCustomerId());
				customerResponseDto.setStatus(625);
			}

		}
		return customerResponseDto;
	}
}
