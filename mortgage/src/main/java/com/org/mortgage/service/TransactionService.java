package com.org.mortgage.service;

import com.org.mortgage.dto.LoanRequestDto;
import com.org.mortgage.dto.LoanResponseDto;
import com.org.mortgage.dto.TransactionResponseDto;
import com.org.mortgage.exception.SavingAccountException;
import com.org.mortgage.exception.SavingAmountException;

public interface TransactionService {
	TransactionResponseDto transactionList(long customerId);

	LoanResponseDto loanPayment(LoanRequestDto loanRequestDto) throws SavingAccountException, SavingAmountException;
}
