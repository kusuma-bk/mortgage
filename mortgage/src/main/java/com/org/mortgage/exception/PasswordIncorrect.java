package com.org.mortgage.exception;

public class PasswordIncorrect extends Exception {
	private static final long serialVersionUID = 1L;

	public PasswordIncorrect(String message) {
		super(message);
	}

}
