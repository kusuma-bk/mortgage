package com.org.mortgage.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.org.mortgage.utility.CustomerUtility;
import com.org.mortgage.utility.MortgageUtility;
import com.org.mortgage.utility.UserUtility;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
			org.springframework.http.HttpHeaders headers, HttpStatus status, WebRequest request) {
		if (ex instanceof MethodArgumentNotValidException) {
			MethodArgumentNotValidException exception = (MethodArgumentNotValidException) ex;
			List<String> errorList = exception.getBindingResult().getFieldErrors().stream()
					.map(fieldError -> fieldError.getDefaultMessage()).collect(Collectors.toList());
			ErrorResponse errorDetails = new ErrorResponse("this is a message from handler", errorList,
					UserUtility.VALIDATION_STATUSCODE);

			return super.handleExceptionInternal(ex, errorDetails, headers, status, request);
		}
		return super.handleExceptionInternal(ex, body, headers, status, request);
	}

	@ExceptionHandler(UserException.class)
	public ResponseEntity<ErrorResponse> customerErrorException(UserException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.USER_EXISTS_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(CustomerNotPresent.class)
	public ResponseEntity<ErrorResponse> productsErrorException(CustomerNotPresent ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.CUSTOMER_NOT_PRESENT_ERROR_STATUSCODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(PasswordIncorrect.class)
	public ResponseEntity<ErrorResponse> productsErrorException(PasswordIncorrect ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.PWD_NOT_PRESENT_ERROR_STATUSCODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(SquareFettDetailsException.class)
	public ResponseEntity<ErrorResponse> quantityException(SquareFettDetailsException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.SQUAREFEET_DETAILS_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(UserNotEligibleException.class)
	public ResponseEntity<ErrorResponse> quantityException(UserNotEligibleException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.USER_NOT_ELIGIBLE_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(PropertyValueException.class)
	public ResponseEntity<ErrorResponse> quantityException(PropertyValueException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.PROPERTY_VALUE_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(DateOfBirthException.class)
	public ResponseEntity<ErrorResponse> quantityException(DateOfBirthException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.DATE_OF_BIRTH_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(SalaryException.class)
	public ResponseEntity<ErrorResponse> quantityException(SalaryException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.SALARY_EXCEPTION_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(SavingAccountException.class)
	public ResponseEntity<ErrorResponse> savingsException(SavingAccountException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(MortgageUtility.SAVING_ACCOUNT_NOT_FOUND_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}
	// SavingAmountException

	@ExceptionHandler(SavingAmountException.class)
	public ResponseEntity<ErrorResponse> savingsAmountException(SavingAmountException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(MortgageUtility.SAVING_AMOUNT_NOT_SUFFICIENT_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(PropertyException.class)
	public ResponseEntity<ErrorResponse> quantityException(PropertyException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.PROPERTY_EXCEPTION_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

}
