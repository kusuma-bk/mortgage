package com.org.mortgage.exception;

public class CustomerNotPresent extends Exception {
	private static final long serialVersionUID = 1L;

	public CustomerNotPresent(String message) {
		super(message);
	}

}
