package com.org.mortgage.exception;

public class SavingAccountException extends Exception {
	private static final long serialVersionUID = 1L;

	public SavingAccountException(String message) {
		super(message);
	}

}
