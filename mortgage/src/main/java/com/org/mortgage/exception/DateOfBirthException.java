package com.org.mortgage.exception;

public class DateOfBirthException extends Exception {
	private static final long serialVersionUID = 1L;

	public DateOfBirthException(String message) {
		super(message);
	}

}
