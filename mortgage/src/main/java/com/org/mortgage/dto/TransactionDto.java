package com.org.mortgage.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class TransactionDto {
	private double emiAmonut;
	private Date paymentDate;
	private double savingsAmount;
	private double mortgageAmount;
}
