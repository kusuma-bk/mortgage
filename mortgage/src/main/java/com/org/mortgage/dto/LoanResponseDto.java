package com.org.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoanResponseDto {
	private String message;
	private int statusCode;
}
