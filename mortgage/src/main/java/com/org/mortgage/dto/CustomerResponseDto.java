package com.org.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomerResponseDto {
	private long savingsAccount;
	private double savingsAmount;
	private double mortgageAmount;
	private long userId;
	private int status;
	private long customerId;
	private double emiAmount;
	
}
