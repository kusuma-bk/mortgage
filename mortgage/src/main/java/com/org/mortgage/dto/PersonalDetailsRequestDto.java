package com.org.mortgage.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class PersonalDetailsRequestDto {
	@NotEmpty(message = "User Name should not empty")
	private String userName;
	@NotEmpty(message = "Phone number should not empty")
	@Pattern(regexp = "(^$|[0-9]{10})", message = "phone number must be  10 digits")
	private String phoneNumber;
	//@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull(message = "Please provide a date of birth.")
	private Date dateOfBirth;
	@Size(min = 10, max = 10, message = "PAN card should be size of 10 length")
	private String panCard;
	@NotEmpty(message = "Gender should not empty")
	private String gender;

}
