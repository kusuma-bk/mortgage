package com.org.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoanRequestDto {
	private long customerId;
	private long savingsAccount;
	private double emiAmonut;
}
