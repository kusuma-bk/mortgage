package com.org.mortgage.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TransactionResponseDto {
	private List<TransactionDto> listOfTransaction;
	private int status;
}
