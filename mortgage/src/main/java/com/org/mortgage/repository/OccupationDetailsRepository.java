package com.org.mortgage.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgage.entity.OccupationDetails;

@Repository
public interface OccupationDetailsRepository extends JpaRepository<OccupationDetails, Long> {

	Optional<OccupationDetails> findByUserId(long userId);

}
