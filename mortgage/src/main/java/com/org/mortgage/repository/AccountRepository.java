package com.org.mortgage.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgage.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	Optional<Account> findByUserId(long userId);

	Optional<Account> findBySavingsAccountAndUserId(long savingsAccount, long userId);

}
