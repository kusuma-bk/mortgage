package com.org.mortgage.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgage.entity.Loan;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Long> {

	Optional<Loan> findByUserId(long userId);

}
