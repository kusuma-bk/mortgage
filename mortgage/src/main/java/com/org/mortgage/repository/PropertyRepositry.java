package com.org.mortgage.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgage.entity.Property;

@Repository
public interface PropertyRepositry extends JpaRepository<Property, Long> {

	List<Property> findByUserId(long userId);

	Optional<Property> findByUserIdAndSquareFeetId(long userId, long squareFeetId);

}
