package com.org.mortgage.utility;

public class UserUtility {
	private UserUtility() {
		throw new IllegalStateException("UserUtility class");
	}

	public static final String USER_PERSONAL_DETAILS = "Please enter Occupational details";
	public static final int USER_PERSONAL_DETAILS_STATUS = 700;

	public static final String USER_OCCUPATIONAL_DETAILS = "Please enter your Property details";
	public static final int USER_OCCUPATIONAL_DETAILS_STATUS = 701;

	public static final String USER_EXISTS_ERROR = "User doesnot exists";
	public static final int USER_EXISTS_ERROR_STATUS = 800;

	public static final String USER_ALREADY_EXISTS_ERROR = "User already exists";
	public static final int USER_ALREADY_EXISTS_ERROR_STATUS = 801;

	public static final String SQUAREFEET_DETAILS_ERROR = "Pin code mismatch";
	public static final int SQUAREFEET_DETAILS_ERROR_STATUS = 606;

	public static final String USER_NOT_ELIGIBLE_ERROR = "Your are not eligiable for this loan";
	public static final int USER_NOT_ELIGIBLE_ERROR_STATUS = 607;

	public static final String LOGIN_SUCCESS = "Your loggin successfully done";
	public static final int LOGIN_SUCCESS_STATUS = 701;

	public static final String PROPERTY_VALUE_EXCEPTION = "Your property value should greater than your loan amount";
	public static final int PROPERTY_VALUE_STATUS = 702;
	public static final String DATE_OF_BIRTH_EXCEPTION = "Age should greater than 21";
	public static final int DATE_OF_BIRTH_STATUS = 703;
	public static final String SALARY_EXCEPTION = "Your salary should greater than emi value";
	public static final int SALARY_EXCEPTION_STATUS = 703;

	public static final int VALIDATION_STATUSCODE = 888;

	public static final String PROPERTY_EXCEPTION = "Property Value and userId  also there";
	public static final int PROPERTY_EXCEPTION_STATUS = 705;

}
