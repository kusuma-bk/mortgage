package com.org.mortgage.utility;

public class MortgageUtility {
	private MortgageUtility() {
		throw new IllegalStateException("MortgageUtility class");
	}

	public static final String SAVING_ACCOUNT_NOT_FOUND = "Account not found";
	public static final int SAVING_ACCOUNT_NOT_FOUND_STATUS = 666;

	public static final String SAVING_AMOUNT_NOT_SUFFICIENT = "Amount not sufficient";
	public static final int SAVING_AMOUNT_NOT_SUFFICIENT_STATUS = 667;
}
