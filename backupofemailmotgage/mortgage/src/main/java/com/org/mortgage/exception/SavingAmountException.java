package com.org.mortgage.exception;

public class SavingAmountException extends Exception {
	private static final long serialVersionUID = 1L;

	public SavingAmountException(String message) {
		super(message);
	}

}
