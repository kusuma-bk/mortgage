package com.org.mortgage.exception;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorResponse {
	private String message;
	private int status;

	private List<String> details;

	public ErrorResponse() {
		super();
	}

	public ErrorResponse(String message, List<String> details2, int status) {
		this.message = message;
		this.details = details2;
		this.status = status;
	}

}
