package com.org.mortgage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Loan {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long loanId;
	private long propertyId;
	
	private double loanAmount;
	private int rateOfInterest;
	private int tenureInMonths;
	private double emiAmount;
	private long userId;
	private double mortgageAmount;

}
