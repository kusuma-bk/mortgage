package com.org.mortgage.service;

import com.org.mortgage.dto.CustomerDto;
import com.org.mortgage.dto.CustomerResponseDto;
import com.org.mortgage.exception.CustomerNotPresent;
import com.org.mortgage.exception.PasswordIncorrect;

public interface CustomerService {
	public CustomerResponseDto customerLogin(CustomerDto customerDto) throws CustomerNotPresent, PasswordIncorrect;
}
