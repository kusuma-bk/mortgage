package com.org.mortgage.exception;

public class PropertyException extends Exception {
	private static final long serialVersionUID = 1L;

	public PropertyException(String message) {
		super(message);
	}

}
