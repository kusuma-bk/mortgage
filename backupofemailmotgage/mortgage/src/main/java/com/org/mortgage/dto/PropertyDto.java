package com.org.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PropertyDto {
	private String propertyName;
	private long area;
	private int pincode;
	private double depositValue;
	private double loan;
	private int tenureMonths;
	private long userId;
}
