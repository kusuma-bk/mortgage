package com.org.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginResponseDto {
	private long loginId;
	private String password;
	private String message;
	private int status;
	
	
}
