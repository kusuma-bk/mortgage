package com.org.mortgage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgage.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByUserId(long userId);

}
