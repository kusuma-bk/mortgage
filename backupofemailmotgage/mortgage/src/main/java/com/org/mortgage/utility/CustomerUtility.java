package com.org.mortgage.utility;

public class CustomerUtility {
	private CustomerUtility() {
		throw new IllegalStateException("CustomerUtility class");
	}

	public static final String CUSTOMER_NOT_PRESENT = "CustomerId is Not Present";
	public static final int CUSTOMER_NOT_PRESENT_ERROR_STATUSCODE = 620;

	public static final String PWD_NOT_PRESENT = "Incorrect Password";
	public static final int PWD_NOT_PRESENT_ERROR_STATUSCODE = 621;
}


