package com.org.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PersonalDetailsResponseDto {

	private String message;
	private int statusCode;
	private long userId;

}
