package com.org.mortgage.exception;

public class SquareFettDetailsException extends Exception {
	private static final long serialVersionUID = 1L;

	public SquareFettDetailsException(String message) {
		super(message);
	}

}
