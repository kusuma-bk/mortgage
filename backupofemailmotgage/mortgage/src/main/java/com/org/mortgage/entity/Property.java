package com.org.mortgage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Property {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long propertyId;
	private String propertyName;
	private double propertyValue;
	private long squareFeetId;
	private long userId;

}
