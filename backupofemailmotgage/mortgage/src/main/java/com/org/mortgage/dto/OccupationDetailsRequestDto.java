package com.org.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OccupationDetailsRequestDto {

	private double salary;
	private String designation;
	private String organizationName;
	private String organizationAddress;
	private long userId;
}
