package com.org.mortgage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.mortgage.dto.CustomerDto;
import com.org.mortgage.dto.CustomerResponseDto;
import com.org.mortgage.exception.CustomerNotPresent;
import com.org.mortgage.exception.PasswordIncorrect;
import com.org.mortgage.service.CustomerService;

@RequestMapping("/login")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LoginController {

	@Autowired
	CustomerService customerService;

	@PostMapping
	public ResponseEntity<CustomerResponseDto> customerLogin(@RequestBody CustomerDto customerDto)
			throws CustomerNotPresent, PasswordIncorrect {

		return new ResponseEntity<>(customerService.customerLogin(customerDto), HttpStatus.OK);
	}
}
